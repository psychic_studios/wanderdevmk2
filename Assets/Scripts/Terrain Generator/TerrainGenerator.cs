﻿using UnityEngine;
using System.Collections;

public class TerrainGenerator : MonoBehaviour {

    [Header("Generation Customisation")]
    // Seed is the place on the perlin noise plane where terrain will be taken from.
    public Vector2 seed;
    // World size. Later it will be used when terrain is 100% procedurally generated.
    public int RenderDistance;
    // Scale of the world. Smaller means things are spread out more, bigger means things
    // are closer together
    public float Scale;
    // Player that terrain will be generated around
    public GameObject Player;

    [Header("Tiles")]
    public GameObject GrassTile;
    public GameObject SandTile;
    public GameObject WaterTile;
    public GameObject Tree;
    

	void Start () {

        Random.seed = Random.Range(-1000, 1000);

        // if seed is -1, -1, then generate a random seed.
        if (seed == new Vector2(-1, -1))
        {
            seed = new Vector2(
                (float)Random.Range((float)-1000f, (float)1000f), 
                (float)Random.Range((float)-1000f, (float)1000f));
        }

        // Start generating terrain
        StartCoroutine(Generate());
	}

    private IEnumerator Generate()
    {
        // Make rows of blocks
        for (int x = 0; x <= RenderDistance; x++)
        {
            // Wait 'till end of frame, otherwise, your PC will be 100% frozen
            // while generating terrain.
            yield return new WaitForEndOfFrame();

            // Populate the rows of blocks with tiles
            for (int y = 0; y <= RenderDistance; y++)
            {

                // Make a new cube.
                GameObject newTile = (GameObject)Instantiate(WaterTile);

                // Place the cube in its spot.
                newTile.transform.position = new Vector2(x, y);

                // Seed is the spot on the perlin noise plane
                // Cube's position will be divided by scale (Incase you want islands and trees really spread out or close together)
                // Dividing by render distance makes it so that it is always between 0 and 1
                float perlinX = /*Mathf.Clamp(*/seed.x + (float)x / RenderDistance * Scale;//, 0, 1);
                float perlinY = /*Mathf.Clamp(*/seed.y + (float)y / RenderDistance * Scale;//, 0, 1);

                float height = Mathf.PerlinNoise(perlinX, perlinY);

                float perlinHeight = height;

                height *= 10;
                height = Mathf.RoundToInt(height);

                // If height is lower than 5, keep it as water
                if (height > 5)
                {
                    // If height is bigger than 5.5 and smaller than 6, make it sand!
                    // Y U NO WORK?!
                    if (height > 6)
                    {
                        GameObject newTile2 = (GameObject)Instantiate(GrassTile);
                        newTile2.transform.position = newTile.transform.position;
                        newTile2.GetComponent<SpriteRenderer>().color = new Color(0.1f + perlinHeight, 0.1f + perlinHeight, 0.1f + perlinHeight);
                        newTile2.gameObject.name = "GRASS " + height + "/" + perlinHeight;
                        Destroy(newTile);

                        if (Random.Range(23, 27) == 25)
                        {
                            GameObject tree = (GameObject)Instantiate(Tree);
                            tree.transform.position = new Vector3(newTile2.transform.position.x - 1f, newTile2.transform.position.y + 1f, newTile2.transform.position.z + 1);
                        }
                    }
                    // Valid if statement, everything looks fine
                    // if height is in the range of .55 and .6
                    else if (!(perlinHeight > 0.5f && perlinHeight < 0.6f))
                    {
                        GameObject newTile2 = (GameObject)Instantiate(SandTile);
                        newTile2.transform.position = newTile.transform.position;
                        newTile2.GetComponent<SpriteRenderer>().color = new Color(0.1f + perlinHeight, 0.1f + perlinHeight, 0.1f + perlinHeight);
                        newTile2.gameObject.name = "SAND " + height + "/" + perlinHeight;
                        Destroy(newTile);
                    }
                    
                    
                }
                else
                {
                    newTile.gameObject.name = "WATER " + height + "/" + perlinHeight;
                }
            }
        }
    }
}
