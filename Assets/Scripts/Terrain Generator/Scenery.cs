﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scenery : MonoBehaviour {

    [Header("Biomes")]
    public bool isBiomeSpecific = true;
    public List<string> biomes;

    [Header("Blocks")]
    public bool isBlockSpecific = true;
    public List<GameObject> blocks;

    [Header("Rarity")]
    public int Rarity = 50;

    [Header("Texture")]
    public Sprite[] textures;

    [Header("Item Dropped name: Please enter the name of the item this drops")]
    public string itemDropName;


    [Header("Player Ineraction stats, please dont tough drop")]
    public bool canBreak;
    public float toughness;
    public string requires;
    public Item drop;
    public Scenery replaceWith;
    public ItemDatabase itemDatabase;

    [Header("Display Stuff")]
    public string hovertext = "";
    private bool hovering = false;
    public float clickheight;

    void Start()
    {
        itemDatabase = GameObject.FindGameObjectWithTag("item_Database").GetComponent<ItemDatabase>();
        drop = itemDatabase.findItemByName(itemDropName);
    }

    void OnGUI()
    {
        if (hovering)
        {
            Vector2 mypos = Camera.main.WorldToScreenPoint(transform.position);
            GUI.Box(new Rect(mypos.x, Screen.height - mypos.y, hovertext.Length * 12, 18), "");
            GUI.Label(new Rect(mypos.x, Screen.height - mypos.y, hovertext.Length * 12, 18), hovertext);
        }

    }

    void OnMouseEnter()
    {
        hovering = true;
    }

    void OnMouseExit()
    {
        hovering = false;
    }

    public Item onPlayerInteraction()
    {
        toughness -= 10;
        if(toughness<= 0)
        {
            if(replaceWith!=null)
            {
                Instantiate(replaceWith, this.transform.position, Quaternion.identity);
            }
            if (drop != null)
            {
                return drop;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    /*
    public void playerBreak(Inventory inventory)
    {
        if (canBreak && inventory.equippedWeapon != null && inventory.equippedWeapon.itemID.Contains(requires) && inventory.equippedWeapon.damage >= toughness)
        {
            foreach (Item item in drops)
                inventory.addItem(item);
            if (replaceWith != null)
            {
                GameObject newScenery = Instantiate(replaceWith.gameObject);
                newScenery.transform.position = this.transform.position;
                //newScenery.transform.parent = this.transform;
                newScenery.GetComponent<SpriteRenderer>().sortingOrder =
                                        -((int)newScenery.transform.position.y - (int)(newScenery.GetComponent<SpriteRenderer>().sprite.textureRect.height * newScenery.transform.localScale.y / newScenery.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit / 2));

                if (newScenery.GetComponent<SpriteRenderer>().sortingOrder > 0) newScenery.GetComponent<SpriteRenderer>().sortingOrder--;
            }
            GameObject.Destroy(gameObject);
        }
    }
    */
}
