﻿using UnityEngine;
using System.Collections;

public class ProceduralGeneration : MonoBehaviour {

    private GameObject worldGO;
    private TerrainGeneratorOptimized terrainScript;

    public static bool islandDeleted = false;

    public static int islandsToMake = -2;

	// Use this for initialization
	void Start () {
        StartCoroutine(InitialGeneration());
        worldGO = GameObject.Find("World");
        terrainScript = worldGO.GetComponent<TerrainGeneratorOptimized>();
	}

    void Update()
    {
        if (islandsToMake == 0) { islandsToMake--; terrainScript.smoothTerrain(); }
    }

    private IEnumerator InitialGeneration()
    {
        yield return new WaitForSeconds(0.1f);

        for (int i = 0; i < 1; i++)
        {
            while (terrainScript.isGenerating)
            {
                yield return new WaitForEndOfFrame();
            }

            islandsToMake++;
            terrainScript.islandSeed = new Vector2((float)Random.Range(-1000f, 1000f), (float)Random.Range(-1000f, 1000f));
            StartCoroutine(terrainScript.Generate(Random.Range(-500, 500), Random.Range(-500, 500)));

            if (islandDeleted)
                i--;
            islandDeleted = false;
        }
    }
	
}
