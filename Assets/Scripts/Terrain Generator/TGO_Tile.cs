﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TGO_Tile : MonoBehaviour {

	// HEIGHTRANGE
    [Header("Heightrange")]
    //height ranges are based on X = min height and Y = max height
    public Vector2[] heightRanges;

    // BIOMES
    [Header("Biomes")]
    public bool isBiomeSpecific = false;
    public string[] biomes;

    [Header("Do Not Touch!")]
    public float actualHeight;
    public int id;
    public int x;
    public int y;
    public string biome;

    public TGO_Tile[] neighbors = new TGO_Tile[4];

}
