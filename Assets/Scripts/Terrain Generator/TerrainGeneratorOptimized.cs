﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TerrainGeneratorOptimized : MonoBehaviour {

    [Header("Generation Customisation")]

    // Seed is the place on the perlin noise plane where terrain will be taken from.
    public Vector2 islandSeed;

    // World size. Later it will be used when terrain is 100% procedurally generated.
    public int islandSize;

    // Scale of the world. Smaller means things are spread out more, bigger means things
    // are closer together
    public float islandScale;

    [Header("Player")]

    // The player to be spawned
    public GameObject player;

    [Header("Tiles")]

    // The list of available tiles that can spawn
    public List<GameObject> tileList = new List<GameObject>();

    [Header("Scenery")]

    // The list of available scenery that can spawn
    public List<GameObject> sceneryList = new List<GameObject>();

    [Header("Biomes")]

    // The list of available biomes that islands can be
    public List<string> biomeList = new List<string>();

    // PRIVATE VARIABLES
    private bool playerSpawned = false;

    // A 2D list for containing all the blocks
    private List<List<TGO_Tile>> tileArray = new List<List<TGO_Tile>>();
    private TGO_Tile emptyTile;

    [Header("DEBUG")]
    // For custom seeds.
    public bool doCustomSeeds = false;
    public List<Vector3> seeds = new List<Vector3>();

    [Header("Do not touch!")]
    public bool isGenerating = false;
    public int id = 0;

    // Stores tiles for overlap checking
    public List<Vector3> loadedTileList = new List<Vector3>();

	void Start () {

        // if seed is -1, -1, then generate a random seed.
        if (islandSeed == new Vector2(-1, -1))
        {
            islandSeed = new Vector2(
                (float)Random.Range((float)-1000f, (float)1000f), 
                (float)Random.Range((float)-1000f, (float)1000f));
        }

        // Create an islands gameObject
        GameObject islandGO = new GameObject();
        islandGO.transform.parent = GameObject.Find("World").transform;
        islandGO.name = "Islands";

        emptyTile = new TGO_Tile()
        {
            isBiomeSpecific = false,
        };
	}

    public IEnumerator Generate(float xpos, float ypos)
    {
        if (ProceduralGeneration.islandsToMake == -1) ProceduralGeneration.islandsToMake = 1;

        isGenerating = true;

        bool markToDelete = false;

        List<Vector3> tilesToAdd = new List<Vector3>();
        // Make sure tileList is ordered by lowest minimum spawn height to heighest
        // that way if any tiles' hieght ranges intersect with each other, the heighest
        // tile spawns last and is in view
        //tileList = tileList.OrderBy(o => o.GetComponent<TGO_Tile>().minimumHeight).ToList();

        if (doCustomSeeds) islandSeed = new Vector2(seeds[0].x, seeds[0].y);

        // Choose a random biome for this island to be
        string currentBiome = biomeList[Random.Range(0, biomeList.Count)];

        if (doCustomSeeds) currentBiome = biomeList[(int)seeds[0].z];

        // Create an island gameobject for all the tiles to be parented to
        GameObject islandGO = new GameObject();
        islandGO.transform.parent = GameObject.Find("Islands").transform;
        islandGO.name = currentBiome + " | " + islandSeed;

        // Make rows of blocks
        for (int x = 0; x <= islandSize; x++)
        {
            List<TGO_Tile> xTileArray = new List<TGO_Tile>();
            tileArray.Add(xTileArray);

            // Wait 'till end of frame, otherwise, your PC will be 100% frozen
            // while generating terrain.
            yield return new WaitForEndOfFrame();

            // Populate the rows of blocks with tiles
            for (int y = 0; y <= islandSize; y++)
            {
                // Seed is the spot on the perlin noise plane
                // Cube's position will be divided by scale (Incase you want islands and trees really spread out or close together)
                float perlinX = (islandSeed.x + (float)x / islandSize * islandScale
                    + (islandSeed.x + (float)x / islandSize * islandScale * 1.2f)) / 2;
                float perlinY = (islandSeed.y + (float)y / islandSize * islandScale
                    + (islandSeed.y + (float)y / islandSize * islandScale * 1.2f)) / 2;

                // Get height from the perlin noise plane at perlinX and perlinY
                float perlinHeight = Mathf.PerlinNoise(perlinX, perlinY);

                // Decrease the height of each tile if it's farther away from the center
                // This will make 1 central island and a bunch of smaller ones
                perlinHeight -= Vector2.Distance(new Vector2(x, y), new Vector2(islandSize / 2, islandSize / 2)) / 150f;

                // For each tile in tileList
                for (int i = 0; i < tileList.Count; i++)
                {
                    GameObject temp = tileList[i];
                    int randomIndex = Random.Range(i, tileList.Count);
                    tileList[i] = tileList[randomIndex];
                    tileList[randomIndex] = temp;
                }
                for (int gnum = 0; gnum < tileList.Count; gnum++)
                {
                    GameObject g = tileList[gnum];
                    // Bool if all conditions are met
                    bool generate = false;

                    // Tile from GameObject that contains all the information about
                    // generation
                    TGO_Tile tile = g.GetComponent<TGO_Tile>();

                    // If height is in range
                    foreach(Vector2 range in tile.heightRanges)
                        if ((perlinHeight > range.x && perlinHeight < range.y))
                        {
                            // If this tile is biome specific
                            if (tile.isBiomeSpecific)
                            {
                                // If the island is the correct biome for this tile to spawn
                                if (tile.biomes.Contains<string>(currentBiome))
                                {
                                    // Then all spawning conditions are met and we allow the tile to spawn
                                    generate = true;
                                }
                            }
                            else
                            {
                                // Then all spawning conditions are met and we allow the tile to spawn
                                generate = true;
                            }
                        }

                    // If all the conditions for generation are met
                    if (generate && !markToDelete)
                    {
                        id++;

                        // Create a new tile
                        GameObject newTile = (GameObject)Instantiate(g);
                        TGO_Tile component = newTile.GetComponent<TGO_Tile>();

                        component.actualHeight = perlinHeight;
                        component.x = x;
                        component.y = y;
                        component.id = id;
                        component.biome = currentBiome;


                        //if (tileArray[x].Count != 1 && tileArray[x].Count > y - 1 && tileArray[x][y + 1] != emptyTile)

                        // TRYING TO MAKE THE WORLD HAVE DEPTH

                        /*{
                            Debug.Log(xTileArray[y - 1].id);
                            if (xTileArray[y - 1].actualHeight < perlinHeight)
                            {
                                ypos += 0.1f;
                            } 
                        }*/

                        xTileArray.Add(component);

                        // Set its position
                        newTile.transform.position = new Vector2(xpos + x, ypos + y);

                        // Make it a parent of this island transform
                        newTile.transform.parent = islandGO.transform;

                        // Choose a random number between 0 and 100, and if it equals 50 AND if the player hasn't
                        // already spawned, spawn the player
                        if (perlinHeight > component.heightRanges[0].x + 0.2 && !playerSpawned)
                        {
                            // Move the player to this tile
                            player.transform.position = newTile.transform.position;

                            // And make sure to tell the generator that the player already spawned
                            playerSpawned = true;
                        }
                        if (loadedTileList.Contains(newTile.transform.position))
                        {
                            markToDelete = true;
                        }
                        if (tilesToAdd.Contains(newTile.transform.position))
                        {
                            GameObject.Destroy(newTile);
                            break;
                        }
                        else
                        {
                            tilesToAdd.Add(newTile.transform.position);
                        }

                        World.getInstance().setTile((int)newTile.transform.position.x, (int)newTile.transform.position.y, component);

                        // A bool representing if all conditions for spawning are met
                        bool generateScenery = true;

                        // For each scenery in sceneryList
                        for (int snum = 0; snum <= sceneryList.Count - 1; snum++)
                        {
                            // Make a new scenery object
                            GameObject s = sceneryList[snum];

                            // Get it's scenery class
                            Scenery scenery = s.GetComponent<Scenery>();

                            // If scenery is biome specific
                            if (scenery.isBiomeSpecific)
                            {
                                // If scenery's biomes isn't the current biome
                                if (!scenery.biomes.Contains<string>(currentBiome))
                                {
                                    // Don't generate scenery
                                    generateScenery = false;
                                }
                            }

                            // If scenery is block specific
                            if (scenery.isBlockSpecific)
                            {
                                // If scenery's blocks isn't the current block
                                if (!scenery.blocks.Contains<GameObject>(g))
                                {
                                    // Don't generate scenery
                                    generateScenery = false;
                                }
                            }

                            // Spawn the scenery based on the rarity of the scenery
                            if (Random.Range(0, scenery.Rarity + 1) != (scenery.Rarity + 1) / 2)
                            {
                                generateScenery = false;
                            }

                            // If all conditions for making scenery is met, make it.
                            if (generateScenery)
                            {
                                // Duplicate the selected scenery object
                                GameObject newScenery = (GameObject)Instantiate(s);

                                // Move the new object to the tile's position
                                newScenery.transform.position = new Vector2(xpos + x, ypos + y);

                                // Make sure the scenery's parent is the island
                                newScenery.transform.parent = islandGO.transform;

                                // WIP
                                newScenery.GetComponent<SpriteRenderer>().sortingOrder =
                                        -((int)newScenery.transform.position.y - (int)(newScenery.GetComponent<SpriteRenderer>().sprite.textureRect.height * newScenery.transform.localScale.y / newScenery.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit / 2));

                                if (newScenery.GetComponent<SpriteRenderer>().sortingOrder > 0) newScenery.GetComponent<SpriteRenderer>().sortingOrder--;
                                break;
                            }

                            generateScenery = true;

                        }

                        break;
                    }
                    else
                    {
                        tileArray[x].Add(emptyTile);
                    }
                }
            }
        }

        isGenerating = false;
        if (doCustomSeeds) seeds.RemoveAt(0);
        if (markToDelete)
        {
            GameObject.Destroy(islandGO);
            ProceduralGeneration.islandDeleted = true;
        }
        else
        {

            Debug.Log("New island generated.");
            foreach (Vector3 tile in tilesToAdd)
                loadedTileList.Add(tile);
        }
        ProceduralGeneration.islandsToMake--;
    }

    public void smoothTerrain()
    {
        World world = World.getInstance();
        foreach (SortedList<int, TGO_Tile> x in world.loadedTileList.Values)
            foreach (TGO_Tile y in x.Values)
            {
                y.neighbors = new
                    TGO_Tile[] { world.getTile(y.x, y.y + 1), world.getTile(y.x - 1, y.y), world.getTile(y.x + 1, y.y), world.getTile(y.x, y.y - 1) };
            }
    }
}