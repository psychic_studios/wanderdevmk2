﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public string name;
    public float minHeight;
    public float maxHeight;
    public string spritePath;
    public int r;
    public int g;
    public int b;
    public List<string> biomes = new List<string>();
}

