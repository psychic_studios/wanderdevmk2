﻿using UnityEngine;
using System.Collections.Generic;

public class Biome : MonoBehaviour
{
    public string name;
    public int rarity;
    public float daytimeTemp;
    public float nighttimeTemp;
    public float rainfall;
}