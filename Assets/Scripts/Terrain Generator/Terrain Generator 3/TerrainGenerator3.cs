﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class TerrainGenerator3 : MonoBehaviour {

    public Vector2 islandSeed;
    public int     islandScale;
    public float   islandSize;
    public TextAsset tilesXML;
    public TextAsset biomesXML;
    public float minTileHeight;

    private List<List<Tile>> tileMatrix = new List<List<Tile>>();
    private List<Tile> possibleTiles = new List<Tile>();
    private List<Biome> possibleBiomes = new List<Biome>();

    void Start()
    {
        LoadTiles();
    }

    //
    // TODO: BIOMES
    //

    void LoadBiomes()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(biomesXML.text);

        foreach (XmlElement element in xmlDoc.GetElementsByTagName("Biome"))
        {
            Biome b = new Biome();
            b.name = element.GetAttribute("name");
            b.daytimeTemp = int.Parse(element.GetAttribute("daytimeTemperature").Trim());
            b.nighttimeTemp = int.Parse(element.GetAttribute("nighttimeTemperature").Trim());
            b.rainfall = int.Parse(element.GetAttribute("rainfall").Trim());
            b.rarity = int.Parse(element.GetAttribute("rarity").Trim());
        }
    }

    void LoadTiles()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(tilesXML.text);

        minTileHeight = float.Parse(xmlDoc.GetElementsByTagName("Tiles")[0].Attributes.GetNamedItem("minimumheight").InnerText);

        foreach (XmlElement element in xmlDoc.GetElementsByTagName("Tile"))
        {
            Tile t = new Tile();
            t.name = element.GetAttribute("name").Trim();
            t.minHeight = float.Parse(element.GetAttribute("minHeight").Trim());
            t.maxHeight = float.Parse(element.GetAttribute("maxHeight").Trim());
            t.r = int.Parse(element.GetAttribute("rgbSide").Split(" ".ToCharArray())[0]);
            t.g = int.Parse(element.GetAttribute("rgbSide").Split(" ".ToCharArray())[1]);
            t.b = int.Parse(element.GetAttribute("rgbSide").Split(" ".ToCharArray())[2]);
            t.biomes.Clear();
            foreach (string biome in element.GetAttribute("biomes").Split(" ".ToCharArray()))
            {
                t.biomes.Add(biome);
            }
        }
    }

    void Generate(int xOffset, int yOffset)
    {
        GameObject island = new GameObject();
        Biome biome = island.AddComponent<Biome>();
        biome = Instantiate<Biome>(possibleBiomes[Random.Range(0, possibleBiomes.Count + 1)]);

        island.name = biome.name.ToUpper();

        for (int x = 0; x <= islandSize; x++)
        {
            List<Tile> xRow = new List<Tile>();
            tileMatrix.Add(xRow);

            for (int y = 0; y <= islandSize; y++)
            {

                float perlinHeight = getPerlinHeight(x, y);

                if (perlinHeight < minTileHeight)
                {
                    continue;
                }

                foreach (Tile t in possibleTiles)
                {
                    if (canGenerateTile(t, perlinHeight, biome))
                    {

                    }
                }
            }
        }
    }

    bool canGenerateTile(Tile t, float perlinHeight, Biome biome)
    {
        bool can = true;

        if (perlinHeight < t.minHeight)
        {
            can = false;
        }

        if (perlinHeight > t.maxHeight)
        {
            can = false;
        }

        if (!t.biomes.Contains(biome.name))
        {
            can = false;
        }

        return can;
    }

    float getPerlinHeight(int x, int y)
    {
        float perlinX = islandSeed.x + (float)x / islandSize * islandScale;
        float perlinY = islandSeed.y + (float)y / islandSize * islandScale;

        float perlinHeight = Mathf.PerlinNoise(perlinX, perlinY);

        perlinHeight -= Vector2.Distance(new Vector2(x, y), new Vector2(islandSize / 2, islandSize / 2)) / 150f;

        return perlinHeight;
    }

    /*  
        float perlinX = islandSeed.x + (float)x / islandSize * islandScale;
        float perlinY = islandSeed.y + (float)y / islandSize * islandScale;

        // Get height from the perlin noise plane at perlinX and perlinY
        float perlinHeight = Mathf.PerlinNoise(perlinX, perlinY);

        // Decrease the height of each tile if it's farther away from the center
        // This will make 1 central island and a bunch of smaller ones
        perlinHeight -= Vector2.Distance(new Vector2(x, y), new Vector2(islandSize / 2, islandSize / 2)) / 150f;
      
    */

}
