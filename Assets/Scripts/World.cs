﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World : MonoBehaviour {

    static World worldObj;

    public SortedList<int, SortedList<int, TGO_Tile>> loadedTileList = new SortedList<int, SortedList<int, TGO_Tile>>();

    public TGO_Tile getTile(int x, int y){
        SortedList<int, TGO_Tile> var;
        TGO_Tile var2;
        loadedTileList.TryGetValue(x, out var);
        if (var == null) return null;
        var.TryGetValue(y, out var2);
        return var2;
    }

    public void setTile(int x, int y, TGO_Tile tile)
    {
        SortedList<int, TGO_Tile> var;
        if (!loadedTileList.TryGetValue(x, out var))
            var = new SortedList<int, TGO_Tile>();
        if (var.ContainsKey(y))
            var.Remove(y);
        var.Add(y, tile);
        if (var.Count == 1)
            loadedTileList.Add(x, var);
    }

    public static World getInstance(){
        return worldObj;
    }

	// Use this for initialization
	void Start () {
        worldObj = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void restart()
    {
        Application.LoadLevel(0);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
