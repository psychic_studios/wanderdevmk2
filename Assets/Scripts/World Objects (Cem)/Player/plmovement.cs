﻿using UnityEngine;
using System.Collections;

public class plmovement : MonoBehaviour {
    private Vector3 movementvector;
    private int vertical, horizontal;
	// Use this for initialization
	void Start () {
	
	}
	
    public void MoveTowards(int direction)
    {
        //if (direction == GetComponent<Animator>().GetInteger("Direction"))
        //{
            switch (direction)
            {
                case 0:
                    if (horizontal < 500)
                        horizontal+=25;
                    break;
                case 1:
                    if (vertical < 500)
                        vertical+=25;
                    break;
                case 2:
                    if (horizontal > -500)
                        horizontal-=25;
                    break;
                case 3:
                    if (vertical > -500)
                        vertical-=25;
                    break;
                default:
                    break;
            }
        //}
        Friction(direction);
        GetComponent<Animator>().SetInteger("Direction", direction);
        movementvector = new Vector3(horizontal * 0.0002F, vertical * 0.0002F, 0);
        GetComponent<Animator>().SetInteger("VerticalMovement", vertical);
        GetComponent<Animator>().SetInteger("HorizontalMovement", horizontal);
        transform.position += movementvector;
    }
    void Stop()
    {
        movementvector = Vector3.zero;
        GetComponent<Animator>().SetInteger("VerticalMovement",0);
        GetComponent<Animator>().SetInteger("HorizontalMovement",0);
    }
    void Friction (int dir)
    {
        switch (dir)
        {
            case 0:
                if (horizontal < 0)
                    horizontal++;
                if (vertical > 0)
                    vertical--;
                else if (vertical < 0)
                    vertical++;
                break;
            case 1:
                if (horizontal < 0)
                    horizontal++;
                else if (horizontal > 0)
                    horizontal--;
                if (vertical < 0)
                    vertical++;
                break;
            case 2:
                if (horizontal > 0)
                    horizontal--;
                if (vertical > 0)
                    vertical--;
                else if (vertical < 0)
                    vertical++;
                break;
            case 3:
                if (horizontal < 0)
                    horizontal++;
                else if(horizontal > 0)
                    horizontal--;
                if (vertical > 0)
                    vertical--;
                break;
        }
        GetComponent<Animator>().SetInteger("Direction", dir);
        movementvector = new Vector3(horizontal * 0.0002F, vertical * 0.0002F, 0);
        GetComponent<Animator>().SetInteger("VerticalMovement", vertical);
        GetComponent<Animator>().SetInteger("HorizontalMovement", horizontal);
    }

	// Update is called once per frame
	void Update () {
	    if (Input.GetKey(KeyCode.S))
        {
            MoveTowards(3);
        }
        else if (Input.GetKey(KeyCode.W))
        {
            MoveTowards(1);
        } else
        {
            movementvector.y = 0;
            vertical = 0;
            GetComponent<Animator>().SetInteger("VerticalMovement", 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            MoveTowards(0);
        } else if (Input.GetKey(KeyCode.A))
        {
            MoveTowards(2);
        }else
        {
            movementvector.x = 0;
            horizontal = 0;
            GetComponent<Animator>().SetInteger("HorizontalMovement", 0);
        }

        
        if (Input.GetKey(KeyCode.None))
        {
            Stop();
        }
	}
}
