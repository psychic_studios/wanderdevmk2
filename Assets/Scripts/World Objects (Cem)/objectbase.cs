﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public abstract class objectbase
    {
    public Sprite mylooks;
    public Vector2 anchor;
    public string Objectname;
    public int maxworldhealth, currentworldhealth;
    public objectbase(Vector2 anch, Sprite look, string n, int wmh)
    {
        mylooks = look;
        anchor = anch;
        Objectname = n;
        currentworldhealth = maxworldhealth;
    }
    public abstract resource getResource();
    public abstract creature getCreature();
    public abstract collectable getCollectable();
    }
