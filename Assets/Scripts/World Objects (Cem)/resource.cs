﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class resource : objectbase
{
    public int loottypenumber, amountfromeach;
    public resource(Vector2 anch, Sprite look, string n, int totalhealth, int lootype, int rewamnt) : base(anch,look,n, totalhealth)
    {
        loottypenumber = lootype;
        amountfromeach = rewamnt;
    }

    public override creature getCreature()
    {
        throw new NotImplementedException();
    }

    public override resource getResource()
    {
        return this;
    }

    public override collectable getCollectable()
    {
        throw new NotImplementedException();
    }
}