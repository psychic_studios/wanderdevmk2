﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

public abstract class aicoder
{

    public abstract IEnumerator WanderAround();
    public abstract IEnumerator Attack(GameObject a);
    public abstract IEnumerator Follow(GameObject a);
    public abstract IEnumerator Wait();
}
