﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class collectable : objectbase
{
    public int loottypenumber, amountfromeach;
    public collectable(Vector2 anch, Sprite look, string n, int totalhealth, int lootype, int rewamnt) : base(anch,look,n, totalhealth)
    {
        loottypenumber = lootype;
        amountfromeach = rewamnt;
    }

    public override creature getCreature()
    {
        throw new NotImplementedException();
    }

    public override resource getResource()
    {
        throw new NotImplementedException();
    }
    public override collectable getCollectable()
    {
        return this;
    }
}