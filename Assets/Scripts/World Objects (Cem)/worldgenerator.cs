﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class worldgenerator : MonoBehaviour {
    public List<objectbase> myresources = new List<objectbase>();
    public List<objectbase> myflowersandstuff = new List<objectbase>();
    public objectbase[][] mytilefills = new objectbase[64][];
    public float celllength;
    public GameObject myprefab, mynoncolprefab;
    public bool[][] filledtile = new bool[64][];
    public GameObject[][] myobjects = new GameObject[64][];
    public TextAsset[] myislands;
    public GameObject tileobject;
    public Texture2D tiletexture;
    public int selectedmap;
    public List<Sprite> mysprlist;
	// Use this for initialization
	void Start () {
        //Read();
        LoadGameObjects();
        for (int a = 0; a < 64; a++)
        {
            filledtile[a] = new bool[64];
            for (int b = 0; b < 64; b++)
            {
                filledtile[a][b] = true;
                GameObject mynexttile = Instantiate(tileobject, new Vector3(a * celllength, b * celllength, 100), Quaternion.identity) as GameObject;
                Sprite mynextsprite = Sprite.Create(tiletexture, new Rect(0, 0, tiletexture.width, tiletexture.width), Vector2.zero);
                mynexttile.transform.localScale = new Vector3(100*celllength/tiletexture.width,100*celllength/tiletexture.height,1);
                mynexttile.GetComponent<SpriteRenderer>().sprite = mynextsprite;
            }
        }
        Randomize();
        Generate();
	}
	
    void LoadGameObjects()
    {
        myresources.Add(new resource(new Vector2(mysprlist[0].rect.width / 2, -4*mysprlist[0].rect.height / 5), mysprlist[0], "Tree1", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[1].rect.width / 2, -4 * mysprlist[1].rect.height / 5), mysprlist[1], "Tree2", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[2].rect.width / 2, -4 * mysprlist[2].rect.height / 5), mysprlist[2], "Tree3", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[3].rect.width / 2, -4 * mysprlist[3].rect.height / 5), mysprlist[3], "Tree4", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[4].rect.width / 2, -4 * mysprlist[4].rect.height / 5), mysprlist[4], "Tree5", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[5].rect.width / 2, -4 * mysprlist[5].rect.height / 5), mysprlist[5], "Tree6", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[6].rect.width / 2, -4 * mysprlist[6].rect.height / 5), mysprlist[6], "Tree7", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[7].rect.width / 2, -4 * mysprlist[7].rect.height / 5), mysprlist[7], "Tree8", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[8].rect.width / 2, -4 * mysprlist[8].rect.height / 5), mysprlist[8], "Tree9", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[9].rect.width / 2, -4 * mysprlist[9].rect.height / 5), mysprlist[9], "Tree10", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[10].rect.width / 2, -4 * mysprlist[10].rect.height / 5), mysprlist[10], "Tree11", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[11].rect.width / 2, -4 * mysprlist[11].rect.height / 5), mysprlist[11], "Tree12", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[12].rect.width / 2, -4 * mysprlist[12].rect.height / 5), mysprlist[12], "Stone1", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[13].rect.width / 2, -4 * mysprlist[13].rect.height / 5), mysprlist[13], "Stone2", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[14].rect.width / 2, -4 * mysprlist[14].rect.height / 5), mysprlist[14], "Stone3", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[15].rect.width / 2, -4 * mysprlist[15].rect.height / 5), mysprlist[15], "Stone4", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[16].rect.width / 2, -4 * mysprlist[16].rect.height / 5), mysprlist[16], "Stone5", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[17].rect.width / 2, -4 * mysprlist[17].rect.height / 5), mysprlist[17], "Stone6", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[18].rect.width / 2, -4 * mysprlist[18].rect.height / 5), mysprlist[18], "Stone7", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[19].rect.width / 2, -4 * mysprlist[19].rect.height / 5), mysprlist[19], "Stone8", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[20].rect.width / 2, -4 * mysprlist[20].rect.height / 5), mysprlist[20], "Stone9", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[21].rect.width / 2, -4 * mysprlist[21].rect.height / 5), mysprlist[21], "Stone10", 4, 1, 5));
        myresources.Add(new resource(new Vector2(mysprlist[22].rect.width / 2, -4 * mysprlist[22].rect.height / 5), mysprlist[22], "Stone11", 4, 1, 5));
    }

    void Read()
    {
        string total = myislands[selectedmap].text;
        int cursor = 0;
        for (int b = 0; b < 64; b ++)
        {
            filledtile[b] = new bool[64];
            for (int c = 0; c < 64; c++)
            {
                if (getWord(total, cursor) == "true")
                {
                    filledtile[b][c] = true;
                }
                else
                {
                    filledtile[b][c] = false;
                }
            }
        }
    }

    string getWord(string phrase, int startloc)
    {
        int a = startloc;
        string result = "";
        while (phrase[a] != ',')
        {
            result += phrase[a];
            a++;
        }
        return result;
    }

    void Randomize()
    {
        for (int a = 0; a < 64; a++)
        {
            mytilefills[a] = new objectbase[64];
            for (int c = 0; c < 64; c++)
            {
                if (UnityEngine.Random.Range(0, 2) != 0)
                {
                    int rand = Random.Range(0, myresources.Count);
                    mytilefills[a][c] = myresources[rand];
                }
            }
        }
    }

    void Generate()
    {
        for (int a = 0; a < 64; a++)
        {
            for (int c = 0; c < 64; c++)
            {
                if (mytilefills[a][c] != null)
                {
                    GameObject b = Instantiate(myprefab, new Vector3(a * celllength, c * celllength, 5), Quaternion.identity) as GameObject;
                    b.GetComponent<worldobject>().Init(mytilefills[a][c]);
                }
            }
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
