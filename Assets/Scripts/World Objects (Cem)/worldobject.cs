﻿using UnityEngine;
using System.Collections;

public class worldobject : MonoBehaviour {
    public objectbase mysource;
    private bool mouseover = false;

	// Use this for initialization
	public void Init (objectbase startup) {
        mysource = startup;
        GetComponent<SpriteRenderer>().sprite = mysource.mylooks;
        //transform.position -= new Vector3(mysource.anchor.x, mysource.anchor.y, 0);
        GetComponent<BoxCollider2D>().offset = mysource.anchor/100;
        GetComponent<BoxCollider2D>().size = new Vector2(mysource.mylooks.rect.width / 20, mysource.mylooks.rect.height / 30);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseEnter()
    {
        mouseover = true;
    }

    void OnMouseExit()
    {
        mouseover = false;
    }
    IEnumerator Chop()
    {
        yield return new WaitForSeconds(0.3F);
        mysource.currentworldhealth--;
        if (Vector2.Distance(transform.position, GameObject.Find("player").transform.position) < 5F)
        {
            if (mysource.currentworldhealth > 0)
            {
                GetComponent<Animator>().SetBool("chopping", true);
                yield return new WaitForSeconds(0.5F);
                GetComponent<Animator>().SetBool("chopping", false);
                StartCoroutine(Chop());
            }
            else
            {
                Destroy(this.gameObject, 0.3F);
            }
        }
    }
    void OnMouseDown()
    {
        if (mysource.getResource().loottypenumber != 0)
        {
            StartCoroutine(Chop());
        }
    }

    void OnGUI()
    {
        if (mouseover)
            GUI.Box(new Rect(Input.mousePosition.x,Screen.height - Input.mousePosition.y,mysource.Objectname.Length * 14,35),mysource.Objectname);
    }
}
