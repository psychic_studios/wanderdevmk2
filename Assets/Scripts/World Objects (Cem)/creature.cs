﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class creature:objectbase
{
    public string creaturetype;
    public aicoder artificalint;
    public creature(Vector2 anch, Sprite look, string n, int totalhealth, string cretype) : base(anch,look,n, totalhealth)
    {
        creaturetype = cretype;
    }
    public IEnumerator ActNew()
    {
        yield return new WaitForSeconds(1F);
        int rand = UnityEngine.Random.Range(0,6);
        switch (rand)
        {
            case 0:
                artificalint.Wait();
                break;
            case 1:
                artificalint.WanderAround();
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
        }
    }

    public override resource getResource()
    {
        throw new NotImplementedException();
    }

    public override creature getCreature()
    {
        return this;
    }

    public override collectable getCollectable()
    {
        throw new NotImplementedException();
    }
}
