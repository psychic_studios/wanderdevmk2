﻿using UnityEngine;
using System.Collections;

public class CameraBob : MonoBehaviour {

    private float camSize;
    private Rigidbody2D rbody;

    void Start()
    {
        camSize = Camera.main.orthographicSize;
        rbody = GetComponent<Rigidbody2D>();
    }

	void Update () {
        if (Input.GetAxis("Sprint") == 1)
        {
            Camera.main.orthographicSize = 
                camSize - (Mathf.PingPong(Time.time, 0.2f) * Mathf.Clamp(rbody.velocity.magnitude, 0f, 1f));
        }
        else if (Input.GetAxis("Crouch") == 1)
        {
            Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, camSize / 1.5f, 0.01f);
        }
        else
        {
            Camera.main.orthographicSize = Mathf.Lerp(
                Camera.main.orthographicSize,
                camSize - (Mathf.PingPong(Time.time, 1f) * Mathf.Clamp(rbody.velocity.magnitude, 0f, 1f)), 0.03f);
        }
        
	}
}
