﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

    public GUISkin myskin;

    public Texture2D thedarkness;
    public float dayalpha, nightdarknessrate, sunadvancementrate;
    public bool midnight;

    public int health;
    public int maxHealth = 100;
    public float healthRegen = 1;
    public HealthBar myhealth = new HealthBar();
    private int[] healthargs = new int[4];

    public int hunger;
    public int maxHunger = 100;
    public float hungerDegen = 18;

    public int thirst;
    public int maxThirst = 100;
    public float thirstDegen = 12;

    public int stamina;
    public int maxStamina = 100;
    public float staminaRegen = 0.5f;

    public int temperature = 0;
    public int comfortTemperature = 0;

    public int intensity = 0;

    private PlayerMovement playerMovement;

    void OnGUI()
    {
        GUI.depth = 1;
        Color myalpha = GUI.color;
        myalpha.a = dayalpha;
        GUI.color = myalpha;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), thedarkness);
        myalpha.a = 1F;
        GUI.color = myalpha;
        GUI.skin = myskin;
        GUI.depth = 0;
        GUI.Label(new Rect(0, Screen.height - 300, 150, 25),"Health: " + health.ToString() + "/" + maxHealth.ToString());
        GUI.Label(new Rect(0, Screen.height - 260, 150, 25),"Hunger: " + hunger.ToString() + "/" + maxHunger.ToString());
        GUI.Label(new Rect(0, Screen.height - 220, 150, 25), "Thirst: " + thirst.ToString() + "/" + maxThirst.ToString());
        GUI.Label(new Rect(0, Screen.height - 180, 150, 25), "Stamina: " + stamina.ToString() + "/" + maxStamina.ToString());
        GUI.Label(new Rect(Screen.width - 150, Screen.height - 300, 150, 100), "Temp: " + temperature.ToString() + "| Comfort: " + comfortTemperature.ToString());
        healthargs[0] = health;
        healthargs[1] = maxHealth;
        healthargs[2] = thirst;
        healthargs[3] = maxThirst;
        myhealth.DrawMe(healthargs);
    }

    void AdvanceTime()
    {
        if (midnight)
        {
            dayalpha -= 0.01F;
            if (dayalpha <= 0)
            {
                midnight = false;
                dayalpha = 0;
            }
        }
        else if (!midnight)
        {
            dayalpha += 0.01F;
            if (dayalpha >= nightdarknessrate)
            {
                midnight = true;
                dayalpha = nightdarknessrate;
            }

        }
    }


    void Start()
    {
        health = maxHealth;
        hunger = maxHunger;
        thirst = maxThirst;
        stamina = maxStamina;

        playerMovement = GetComponent<PlayerMovement>();

        StartCoroutine(RegenHealth());
        StartCoroutine(DegenHunger());
        StartCoroutine(DegenThirst());
        StartCoroutine(RegenStamina());
        if (midnight)
            dayalpha = nightdarknessrate;
        else
            dayalpha = 0;
        InvokeRepeating("AdvanceTime", sunadvancementrate, sunadvancementrate);
    }

    void Update()
    {
        health = Mathf.Clamp(health, 0, maxHealth);
        hunger = Mathf.Clamp(hunger, 0, maxHunger);
        thirst = Mathf.Clamp(thirst, 0, maxThirst);
        stamina = Mathf.Clamp(stamina, 0, maxStamina);

        intensity = 100 - ((health + hunger + thirst) / 3);
    }

    IEnumerator RegenHealth()
    {
        yield return new WaitForSeconds(healthRegen);
        ChangeHealth(1);
        StartCoroutine(RegenHealth());
    }

    IEnumerator DegenHunger()
    {
        yield return new WaitForSeconds(hungerDegen);
        ChangeHunger(-1);
        StartCoroutine(DegenHunger());
    }

    IEnumerator DegenThirst()
    {
        yield return new WaitForSeconds(thirstDegen);
        ChangeThirst(-1);
        StartCoroutine(DegenThirst());
    }

    IEnumerator RegenStamina()
    {
        yield return new WaitForSeconds(staminaRegen);

        if (playerMovement.movementStatus != PlayerMovement.MovementStatus.Sprinting)
        {
            ChangeStamina(1);
        }

        StartCoroutine(RegenStamina());
    }

    public void ChangeHealth(int amount)
    {
        health += amount;
    }

    public void ChangeHunger(int amount)
    {
        hunger += amount;
    }

    public void ChangeThirst(int amount)
    {
        thirst += amount;
    }

    public void ChangeStamina(int amount)
    {
        stamina += amount;
    }

    public void ChangeTemperature(int amount)
    {
        temperature += amount;
    }

 
}
