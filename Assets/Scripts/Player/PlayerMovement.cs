﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    public float speed = 5;
    public MovementStatus movementStatus = MovementStatus.Jogging;

    private Animator anim;
    private Rigidbody2D rbody2d;
    private SpriteRenderer sprite;
    private static int spriteHeight;

    public float noise = 0;
    public float visibility = 0;

    public bool is_Running;

    public enum MovementStatus
    {
        Crouched,
        Jogging,
        Sprinting
    }

    void Start()
    {
        rbody2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        spriteHeight = sprite.sprite.texture.height;
        is_Running = false;
    }

    void Update()
    {
        if (noise > 2)
            noise -= 0.05f;
        else
            noise = 2;

        visibility = 5f;

        if (Input.GetAxis("Sprint") == 1)
        {
            movementStatus = MovementStatus.Sprinting;
            is_Running = true;
        }
        else if (Input.GetAxis("Crouch") == 1)
        {
            movementStatus = MovementStatus.Crouched;
            visibility -= 3;
            noise -= 1;
            is_Running = false;
        }
        else
        {
            movementStatus = MovementStatus.Jogging;
            is_Running = false;
        }

        if (movementStatus == MovementStatus.Crouched)
        {
            rbody2d.AddForce(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * speed * 5 * Time.deltaTime);
            if ((new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"))).magnitude != 0)
            {
                noise += 0.5f;
                noise *= 0.9f;
                visibility += 1;
            }
        }
        else if (movementStatus == MovementStatus.Sprinting)
        {
            rbody2d.AddForce(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * speed * 15 * Time.deltaTime);
            if ((new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"))).magnitude != 0)
            {
                noise += 3f;
                noise *= 0.9f;
                visibility += 4;
            }
        }
        else
        {
            rbody2d.AddForce(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * speed * 10 * Time.deltaTime);
            if ((new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"))).magnitude != 0)
            {
                noise += 1f;
                noise *= 0.9f;
                visibility += 2;
            }
        }

        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            anim.SetInteger("xMovement", 1);
            anim.SetBool("isRunning", is_Running);
        }
        else if (Input.GetAxisRaw("Horizontal") < 0)
        {
            anim.SetInteger("xMovement", -1);
            anim.SetBool("isRunning", is_Running);
        }
        else
        {
            anim.SetInteger("xMovement", 0);
            anim.SetBool("isRunning", is_Running);
        }


        if (Input.GetAxisRaw("Vertical") > 0)
        {
            anim.SetInteger("yMovement", 1);
            anim.SetBool("isRunning", is_Running);
        }
        else if (Input.GetAxisRaw("Vertical") < 0)
        {
            anim.SetInteger("yMovement", -1);
            anim.SetBool("isRunning", is_Running);
        }
        else
        {
            anim.SetInteger("yMovement", 0);
            anim.SetBool("isRunning", is_Running);
        }

        
        sprite.sortingOrder =
            -((int)this.transform.position.y -
            (int)(spriteHeight * this.transform.localScale.y / sprite.sprite.pixelsPerUnit / 2));

    }

    void LateUpdate()
    {

        //sprite.sortingOrder = (int)Camera.main.WorldToScreenPoint(sprite.bounds.min).z * -1;
    }
}
