﻿using UnityEngine;
using System.Collections;

public class CreatureDeer : Creature {

    public void Start()
    {
        ais = new AI[]{
            new AIRunOnSight(GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>(), this)
        };
    }

    public override void runAI()
    {
        ais[0].step();
    }
}
