﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomSpawns : MonoBehaviour {

    public List<Creature> creatures = new List<Creature>();
    GameObject player;
    GameObject creatureGO;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player");
        creatureGO = GameObject.Instantiate(new GameObject());
        creatureGO.transform.parent = GameObject.Find("World").transform;
        creatureGO.transform.name = "Creatures";
        creatureGO.transform.position = Vector3.zero;
        foreach (Transform child in transform)
        {
            creatures.Add(child.GetComponent<Creature>());
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector2 pos;
        pos = (Random.insideUnitCircle * 30) + new Vector2(player.transform.position.x, player.transform.position.y);
        while (Camera.main.WorldToViewportPoint(new Vector3(pos.x, pos.y, 0)).x < 1 &&
            Camera.main.WorldToViewportPoint(new Vector3(pos.x, pos.y, 0)).x > 0 &&
            Camera.main.WorldToViewportPoint(new Vector3(pos.x, pos.y, 0)).y < 1 &&
            Camera.main.WorldToViewportPoint(new Vector3(pos.x, pos.y, 0)).y > 0)
        {
            pos = (Random.insideUnitCircle * 30) + new Vector2(player.transform.position.x, player.transform.position.y);
        }
        TGO_Tile tile = World.getInstance().getTile((int)Mathf.RoundToInt(pos.x), (int)Mathf.RoundToInt(pos.y));
        if (tile != null)
        {
            List<Creature> viableCreatures = new List<Creature>();
            foreach (Creature creature in creatures)
            {
                bool viable = true;
                if (creature.biomeRestrictive)
                    if (!new List<string>(creature.biomes).Contains(tile.biome))
                        viable = false;
                if (creature.tileRestrictive)
                    if (!new List<TGO_Tile>(creature.tiles).Contains(tile))
                        viable = false;
                if (viable)
                    viableCreatures.Add(creature);
            }
            GameObject newCreature;
            if (viableCreatures.Count > 0)
                if (Random.Range(0, 150) == 1)
                {
                    newCreature = GameObject.Instantiate(viableCreatures[Random.Range(0, viableCreatures.Count)].gameObject);
                    newCreature.transform.position = new Vector3(pos.x, pos.y, 0);
                    newCreature.transform.parent = creatureGO.transform;
                    newCreature.GetComponent<SpriteRenderer>().sortingOrder =
                                        -((int)newCreature.transform.position.y - (int)(newCreature.GetComponent<SpriteRenderer>().sprite.texture.height * newCreature.transform.localScale.y / newCreature.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit / 2));

                    if (newCreature.GetComponent<SpriteRenderer>().sortingOrder > 0) newCreature.GetComponent<SpriteRenderer>().sortingOrder--;
                }
        }

	}
}
