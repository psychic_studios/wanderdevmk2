﻿using UnityEngine;
using System.Collections;

public class EnemyLineOfSight : MonoBehaviour {

    private Vector2 lastSeen = Vector2.zero;
    private Rigidbody2D rbody;
    private GameObject target = null;

    public float viewDistance = 10;
    public float attentiveness = 5;
    public float speed = 0.5f;
    public float provokeDistance = 5;
    public bool isProvoked = false;
    public LayerMask layerMask;
    public AIType aiType;

    [HideInInspector]
    private int detected = 0;

    public enum AIType
    {
        Passive,
        Neutral,
        Aggressive
    }

    void Start()
    {
        Application.targetFrameRate = 60;

        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }

        rbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, target.transform.position - transform.position, viewDistance, layerMask);

        if (hit.collider != null && hit.collider.gameObject.tag == target.tag)
        {
            if (detected >= attentiveness * 60)
            {
                lastSeen = hit.collider.gameObject.transform.position;
            }
            else
            {
                detected += 1;
            }

            if (Vector2.Distance(hit.collider.transform.position, transform.position) <= provokeDistance)
            {
                isProvoked = true;
            }
        }
        else
        {
            if (detected > 0)
            {
                detected--;
            }

            if (detected <= 0)
            {
                isProvoked = false;
            }
        }

        if (isProvoked && lastSeen != Vector2.zero)
        {
            if (aiType == AIType.Passive)
            {
                rbody.AddForce((lastSeen - new Vector2(transform.position.x, transform.position.y)).normalized * -speed);
            }
            else if (aiType == AIType.Aggressive)
            {
                rbody.AddForce((lastSeen - new Vector2(transform.position.x, transform.position.y)).normalized * speed);
            }
        }
    }

}
