﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Creature : MonoBehaviour {

    [Header("Spawning Rules")]
    public bool biomeRestrictive = false;
    public string[] biomes;

    public bool tileRestrictive = false;
    public TGO_Tile[] tiles;

    [Header("AIs")]
    public AI[] ais;

    [Header("Stats")]
    public float speed = 1f;

    [Header("DEBUG")]
    public bool aiEnabled = true;

    public abstract void runAI();

    public void Update()
    {
        if (aiEnabled)
            runAI();
    }
}
