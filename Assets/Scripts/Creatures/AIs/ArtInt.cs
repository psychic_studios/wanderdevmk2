﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

public abstract class ArtInt
{
    private int State; // 0: idle, 1: Wandering around, 2: following, 3: defending
    public Animator anim;
    public GameObject thesubject;
    public float mspeed;

    public void Decide()
    {
        State = Random.Range(0, 5);
    }

    public void Act()
    {

    }
    
    void takeAstep(int direction)
    {
        anim.SetBool("walking", true);
        anim.SetInteger("direction", direction);
        switch (direction)
        {
            case 0:
                thesubject.transform.position += new Vector3(mspeed / 4, 0, 0);
                break;
            case 1:
                thesubject.transform.position += new Vector3(0, mspeed / 4, 0);
                break;
            case 2:
                thesubject.transform.position += new Vector3(-mspeed / 4, 0, 0);
                break;
            case 3:
                thesubject.transform.position += new Vector3(0, -mspeed / 4, 0);
                break;
        }

    }

    IEnumerator Wander(bool idle)//oh yes, signature move? maybe.
    {
        yield return new WaitForSeconds(0.8F);
        switch (idle)
        {
            case true: //idle
                yield return new WaitForSeconds(4.3F);
                break;
            case false: //randomdirection
                int direction = Random.Range(0, 4);
                takeAstep(direction);
                yield return new WaitForSeconds(0.8F);
                takeAstep(direction);
                yield return new WaitForSeconds(0.8F);
                takeAstep(direction);
                break;
        }
    }

    void Follow()
    {

    }
}
