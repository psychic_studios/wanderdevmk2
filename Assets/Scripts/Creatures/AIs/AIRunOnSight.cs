﻿using UnityEngine;
using System.Collections;

public class AIRunOnSight : AI {

    public Rigidbody2D rigidBody;
    public bool noticed = false;
    PlayerMovement player;
    Creature creature;

    //

    public AIRunOnSight(PlayerMovement player, Creature creature){
        this.player = player;
        this.creature = creature;
    }

    public override void step()
    {
        if (rigidBody == null)
            rigidBody = creature.GetComponent<Rigidbody2D>();
        if (Vector3.Distance(player.transform.position, creature.transform.position) < player.visibility + (player.noise * 0.3))
        {
            //Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
            //if (GeometryUtility.TestPlanesAABB(planes, creature.GetComponent<Renderer>().bounds)) { }
            
            rigidBody.AddForce((Vector2.MoveTowards(new Vector2(creature.transform.position.x, creature.transform.position.y),
                new Vector2(player.transform.position.x, player.transform.position.y), -Time.deltaTime) - new Vector2(creature.transform.position.x, creature.transform.position.y)).normalized * creature.speed);
        }
    }
}
