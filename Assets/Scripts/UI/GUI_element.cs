﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

    [System.Serializable]
    public abstract class GUI_element
    {
    public Vector2 pos;
    public string Title;
    public bool Movable;

    public abstract void DrawMe(int[] args);
    }

