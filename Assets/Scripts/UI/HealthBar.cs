﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
    public class HealthBar : GUI_element
    {
    public Texture2D barcolor1, barcolor2;
    public int width, barheight;
    public override void DrawMe(int[] args)//arg0:curHP, arg1:thirst
    {
        GUI.Box(new Rect(pos.x, pos.y, width, barheight * 3),"");
        GUI.Label(new Rect(pos.x, pos.y, width, barheight), Title);
        GUI.DrawTexture(new Rect(pos.x, pos.y+ barheight, width * args[0] / args[1], barheight), barcolor1);
        GUI.DrawTexture(new Rect(pos.x, pos.y + barheight*2, width * args[2] / args[3], barheight), barcolor2);
    }

    }
