﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Item {

    public string displayName;
    public int itemID;
    public string itemDescription;
    public Sprite displaySprite;
    public ItemType itemType;
    public int damage = 1;
    public int speed = 1;
    public int weight = 1;
    public GameObject DropItem;
    public GameObject equipItem;
    public bool is_Selected = false;


    public enum ItemType
    {
        Weapon,
        Consumable,
        Material,
        Structure
    }

}
