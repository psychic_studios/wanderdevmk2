﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class Method
{
    public string name;
    public Item first, second, result;
    public Method()
    {
        name = "DoesNotExist";
    }
    public Method(string MethName, Item a, Item b, Item result)
    {
        name = MethName;
        first = a;
        b = second;
        this.result = result;
    }
    public abstract Item[] DoIt();


}


public class Combine : Method
{
    public Combine(string CombName, Item a, Item b, Item result) : base(CombName, a, b, result)
    {
        if (!(first.itemType == Item.ItemType.Material) || !(second.itemType == Item.ItemType.Material))
        {
            CombName = "DoesNotExist";
            first = new Item();
            second = new Item();
            result = new Item();
        }
    }

    public override Item[] DoIt()
    {

        Item[] products = new Item[1];
        if (!(first.itemType == Item.ItemType.Material) || !(second.itemType == Item.ItemType.Material))
        {
            products[0] = new Item();
        }
        else
        {
            products[0] = result;
        }
        return products;

    }
}


public class UseItem : Method
{
    public UseItem(string UseName, Item a, Item b, Item result) : base(UseName, a, b, result)
    {
        if (!((first.itemType == Item.ItemType.Weapon) && (second.itemType == Item.ItemType.Material)))
        {
            UseName = "DoesNotExist";
            first = new Item();
            second = new Item();
            result = new Item();
        }
    }

    public override Item[] DoIt()
    {
        Item[] products = new Item[2];
        products[0] = first;
        products[1] = result;
        return products;
    }
}