﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class MethodDatabase
{
    public List<Method> MethodsList = new List<Method>();

    public Method Get(int a)
    {
        return MethodsList[a];
    }

    public bool Exists(string Mname)
    {
        for (int a = 0; a < MethodsList.Capacity; a++)
        {
            if (MethodsList[a].name == Mname)
                return true;
        }
        return false;
    }

    public Method FindandGet(string Mname)
    {
        for(int a = 0; a < MethodsList.Capacity; a++)
        {
            if (MethodsList[a].name == Mname)
            {
                return MethodsList[a];
            }
            else
                continue;
        }
        return null;
    }

}
