﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class ItemDatabase : MonoBehaviour {
    public List<Item> itemList = new List<Item>();
    
    public Item findItemByName(string name)
    {
        foreach(Item i in itemList)
        {
            if(i.displayName==name)
            {
                return i;
            }
        }
        return null;
    }
    
}
