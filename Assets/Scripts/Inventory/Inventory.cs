﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

    //Basc information about the inventory
    public List<Item> inventory = new List<Item>();
    public List<Item> slots = new List<Item>();
    private ItemDatabase database;
    public bool showInv = true;
    public GUISkin skin;

    //Location and size settings
    public Vector2 inv_Location;
    public int slotsY;
    public int slotsX;
    int itemCount;

    //ToolTip stuff
    private bool showToolTip;
    //private string toolTip;


    //Needed for playing with inventory;
    private bool draggingItem;
    private Item dragItem;
    private int dragIndex;
    private bool draggingInventroy;

    //Needed to modify player Stats
    public PlayerStats stats;

    //Temp for selected item
    private Item selectedItem;
    public Item equippedItem;


    public GameObject player;
	// Use this for initialization
	void Start () {
        database = GameObject.FindGameObjectWithTag("item_Database").GetComponent<ItemDatabase>();
        stats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        player = GameObject.FindGameObjectWithTag("Player");
        slotsY = 5;
        slotsX = 5;
        itemCount = 0;
        int i = 0;
        draggingItem = false;
        draggingInventroy = false;

       
        while (i<slotsX*slotsY)
        {
            inventory.Add(new Item());
            slots.Add(new Item());
            i++;
        }
        addItem("Stone Pickaxe");
        addItem("Stone Axe");
	}

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            showInv = !showInv;
        }   
    }

    void OnGUI()
    {
        GUI.depth = -1;
        if (showInv)
        {
            string toolTip = "";
            int i = 0;
            GUI.skin = skin;
            toolTip = drawInventory();

            Rect selectedRec = new Rect(inv_Location.x + (slotsY * 60), inv_Location.y + 10, 100, 200);
            GUI.Box(selectedRec, createToolTip(selectedItem), skin.GetStyle("ToolTip"));

            if (showToolTip)
            {
                GUI.Box(new Rect(Event.current.mousePosition.x + 15f, Event.current.mousePosition.y, 200, 200), toolTip, skin.GetStyle("ToolTip"));
            }

            if (draggingItem && !draggingInventroy)
            {
                GUI.DrawTexture(new Rect(Event.current.mousePosition.x, Event.current.mousePosition.y, 50, 50), textureFromSprite(dragItem.displaySprite));
            }
            if (Event.current.type == EventType.MouseUp && draggingItem)
            {
                Vector3 worldPos = player.transform.position;
                Debug.Log("Should drop now!");
                Instantiate(dragItem.DropItem, new Vector3(worldPos.x, worldPos.y, 0), Quaternion.identity);
                dragItem = null;
                draggingItem = false;
            }
            if (Event.current.type == EventType.MouseDrag && draggingItem == false && draggingInventroy == false)
            {
                //inv_Location = Event.current.mousePosition;
                draggingInventroy = true;
            }

            if (Event.current.type == EventType.MouseUp && draggingInventroy == true)
            {
                draggingInventroy = false;
            }

            if (draggingInventroy)
            {
                inv_Location = Event.current.mousePosition;
            }
        }

    }

    string drawInventory()
    {
        int loc_X=Mathf.FloorToInt(inv_Location.x);
        int loc_Y=Mathf.FloorToInt(inv_Location.y);
        int placedX=0;
        int placedY=0;
        
        int i = 0;
        string toolTip="";

        //Note that we can add additional styles to modify all the images in this inventory system
        Rect gbRect = new Rect(loc_X, loc_Y, slotsY*60+150, slotsX*60);
        GUI.Box(gbRect, "", skin.GetStyle("Slot"));

        

        while (placedX<slotsX)
        {
            while(placedY<slotsY)
            {
                
                Rect slotRect = new Rect(loc_X + (60 * placedX), loc_Y + (60 * placedY), 50, 50);
                GUI.Box(slotRect,"", skin.GetStyle("Slot"));
                slots[i] = inventory[i];

                if(slots[i].displayName!=null)
                {
                    //This is a problem
                    if (slots[i].displaySprite != null)
                    {
                        GUI.DrawTexture(slotRect, textureFromSprite(slots[i].displaySprite));
                    }
                    else
                    {
                        //Debug.Log("We got a problem");
                    }


                    if (slotRect.Contains(Event.current.mousePosition))
                    {
                        //Draw a tooltip
                        toolTip=createToolTip(slots[i]);
                        showToolTip = true;
                        if (Event.current.button == 0 && Event.current.type == EventType.MouseDrag && !draggingItem && !draggingInventroy)
                        {
                            draggingItem = true;
                            dragIndex = i;
                            dragItem = slots[i];
                            inventory[i] = new Item();
                        }
                        if(Event.current.type == EventType.MouseDown && Event.current.button == 0)
                        {
                            //selectedItem.is_Selected = false;
                            selectedItem = inventory[i];
                            Debug.Log("Should have selected "+ selectedItem.displayName); 
                        }
                        
                        if(Event.current.type==EventType.MouseUp && draggingItem)
                        {
                            
                            inventory[dragIndex] = inventory[i];
                            inventory[i] = dragItem;
                            draggingItem = false;
                            dragItem = null;
                        }
                        if(Event.current.type==EventType.MouseDown && Event.current.button==1)
                        {
                            if(slots[i].itemType==Item.ItemType.Consumable)
                            {
                                //Kill the consumable here
                            }
                        }
                        
                    }
                }
                else
                {
                    if (slotRect.Contains(Event.current.mousePosition))
                    {
                        if (Event.current.type == EventType.MouseUp && draggingItem)
                        {
                            inventory[i] = dragItem;
                            dragItem = null;
                            draggingItem = false;
                        }
                    }
                }

                placedY++;
                i++;
            }
            placedY = 0;
            placedX++;
        }

        //ToolTip Setting
        if(toolTip=="")
        {
            showToolTip = false;
        }

        /*
        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            //selectedItem.is_Selected = false;
            selectedItem = null;
        }
        */
        return toolTip;
    }

    string createToolTip(Item i)
    {
        string toolTip = "";
        if (i == null)
        {
            return toolTip;
        }

        toolTip = "<color=#fffff>" + i.displayName + "</color>\n \n" +
            i.itemDescription;
        return toolTip;
    }


    //This should be moved to a Utils class sometime
    public static Texture2D textureFromSprite(Sprite sprite)
    {
        if (sprite.rect.width != sprite.texture.width)
        {
            Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                         (int)sprite.textureRect.y,
                                                         (int)sprite.textureRect.width,
                                                         (int)sprite.textureRect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }

    public void addItem(string name)
    {
        int i = 0;



        i = 0;
        while(i<inventory.Count)
        {
            if (inventory[i].displayName == null)
            {
                Item item= database.findItemByName(name);
                if(item==null)
                {
                    Debug.Log("Item Not found!");
                    break;
                }
                if (item != null)
                {
                    item.itemID = itemCount;
                    itemCount++;
                    inventory[i] = item;
                }
                break;
            }
            i++;
        }
    }

    public void addItemByItem(Item itm)
    {
        if(itm==null)
        {
            Debug.Log("No item assigned, check your scenery prefabs and the item database");
            return;
        }
        int i = 0;
        while(i<inventory.Count)
        {
            if(inventory[i].displayName==null)
            {
                itm.itemID = itemCount;
                itemCount++;
                inventory[i] = itm;
                break;
            }
            i++;
        }
    }

    public void removeItem(int id)
    {
        foreach(Item i in inventory)
        {
            if(i.itemID==id)
            {
                
                return;
            }
        }
    }

}
