﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAction : MonoBehaviour
{
    public Inventory inv;
    public Camera uiCamera;
    public ItemDatabase items;
    public GameObject player;
    public float clickrange;
    // Use this for initialization
    void Start()
    {
        items = GameObject.FindGameObjectWithTag("item_Database").GetComponent<ItemDatabase>();
        player = GameObject.FindGameObjectWithTag("Player");
        inv = player.GetComponent<Inventory>();
    }

    bool inRange(RaycastHit2D h)
    {
        float a = h.collider.GetComponent<Scenery>().clickheight;
            if (Vector2.Distance(h.transform.position, transform.position - new Vector3(0,a,0)) <= clickrange)
            {
                return true;
            }
            else
                return false;
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Click detected");


            RaycastHit2D[] hits;
            hits = Physics2D.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition).origin, new Vector2(0, 0));

            foreach (RaycastHit2D hit in hits)
            {
                if ((hit.collider != null)&&(inRange(hit)))
                {
                    Debug.Log("Hit something");
                    if (hit.collider.GetComponent<Scenery>() != null)
                    {
                        Item itm=hit.collider.GetComponent<Scenery>().onPlayerInteraction();
                        if (itm == null)
                        {
                            Debug.Log("Item returned null");
                            break;
                        }
                        else
                        {
                            inv.addItemByItem(itm);
                            Destroy(hit.collider.gameObject);
                            break;
                        }
                        
                    }
                }
            }

        }
    }

    
}


