﻿using UnityEngine;
using System.Collections;

public class k_PlayerMovement : MonoBehaviour {
	/* (03APR15) The following code is written for consideration of the game project
	 * that Psychic Games is making for the "Wonder Project".  All code was 
	 * written by Kevin Fogg for the purposes of such and I, Kevin Fogg,
	 * reserve the right to revoke the usage of the following code at any 
	 * point for any reason.  By utilizing the code in the video game, previously named,
	 * the user/programmer understands this and agrees with the terms setforth previously.
	*/

    [Header("Movement Speed")]
    public float moveSpeed = 3f;         //move speed

    [Header("Regen/Degen")]
	public float stamDegen = 10f;			//stamina degeneration during sprinting
	public float stamRegen = 10f;			//stamina regeneration while not sprinting
	public float thirstDegen = 1f;		//thrist degeneration during movement
	public float enerDegen = 3f;			//energy degeneration during movement

    [Header("Timer")]
	public float timer;				//timer to give proper degen/regen

    [Header("Do Not Touch!")]
	public bool isMoving = false;			//bool to help with proper regen/degen of stam
	public bool isSprinting = false;		//bool to help with proper regen/degen of stam
	
	private k_PlayerStats thirsty;		//pulls the thirst from the player stats code
    private k_PlayerStats stamina;		//pulls the stamina from the player stats code
	private k_PlayerStats energy;		//pulls the energy from the player stats code
	
	// Use this for initialization
	void Start () {
		timer = Time.deltaTime;

		stamina = GetComponent<k_PlayerStats> ();
		energy = GetComponent<k_PlayerStats> ();
		thirsty = GetComponent<k_PlayerStats> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown (KeyCode.LeftShift))
		{
			isSprinting = true;
            moveSpeed = moveSpeed * 2;
		}
		
		if (Input.GetKeyUp (KeyCode.LeftShift))
		{
			isSprinting = false;
            moveSpeed = moveSpeed / 2;
		}
		
		if (isMoving == true)
		{
			energy.PlayerEnergy(-enerDegen*timer);
			stamina.PlayerStamina((-stamRegen*timer)/2);
			thirsty.PlayerThirst(-thirstDegen*timer);
			if (isSprinting == true)
			{
				energy.PlayerEnergy((-enerDegen*timer)*2);
				stamina.PlayerStamina(-stamDegen);
				thirsty.PlayerThirst((-thirstDegen*timer)*2);
			}
		}
		
		if (isMoving == false)
		{
			energy.PlayerEnergy((enerDegen*timer)/2);
			stamina.PlayerStamina(stamRegen*timer);
			thirsty.PlayerThirst(thirstDegen*timer);
		}
		
		if (Input.GetKey ("w"))
		{
			isMoving = true;
			transform.Translate((Vector2.up) * moveSpeed * timer);
		}
		if (Input.GetKey ("s")) 
		{
			isMoving = true;
            transform.Translate((-Vector2.up) * moveSpeed * timer);
		} 
		if (Input.GetKey ("a")) 
		{
			isMoving = true;
            transform.Translate((-Vector2.right) * moveSpeed * timer);
		}
		if (Input.GetKey ("d")) 
		{
			isMoving = true;
            transform.Translate((Vector2.right) * moveSpeed * timer);
		}
	}
}
