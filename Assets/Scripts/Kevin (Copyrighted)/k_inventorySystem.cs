﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class k_inventorySystem : MonoBehaviour {
	public List <k_item> inventoryItems = new List <k_item>();
	private k_itemDatabase database;

	// Use this for initialization
	void Start () {
		database = GameObject.FindGameObjectWithTag ("Item Database").GetComponent<k_itemDatabase>();
		inventoryItems.Add(database.items[0]);
	}
	
	void OnGUI()
	{
		for(int i = 0; i < inventoryItems.Count; i++)
		{
			GUI.Label(new Rect(10,i*20, 200, 50), inventoryItems[i].itemName);
		}
	}
}
