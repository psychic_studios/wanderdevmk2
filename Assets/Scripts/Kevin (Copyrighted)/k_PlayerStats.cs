﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class k_PlayerStats : MonoBehaviour {

	/* Health, Thirst, Stamina, Energy, Hunger are the 5 consistantly changing
	 * variables that I have created with this code.
	*/

    [Header("Health")]
	public float health = 100f;
	public float maxHealth;

    [Header("Thirst")]
	public float thirst = 100f;
	public float maxThirst;
	public float thirstDegen = -1f;

    [Header("Stamina")]
	public float stamina = 100f;
	public float maxStamina;

    [Header("Energy")]
	public float energy = 100f;
	public float maxEnergy;

    [Header("Hunger")]
	public float hungry = 100f;
	public float maxHunger;
	public float hungerDegen = -1f;

    [Header("Temperature")]
    public float temperature = 0;
    public float comfortTemp = 0;
    public float adaptingSpeed = 60;

    [Header("Timer")]
	public float timer;

    //public List<>

	void Start () {
		maxHealth = health;
		maxThirst = thirst;
		maxStamina = stamina;
		maxEnergy = energy;
		maxHunger = hungry;
		thirstDegen = thirstDegen * timer;
		hungerDegen = hungerDegen * timer;
		timer = Time.deltaTime;
	}

	public void PlayerStamina (float stam)
	{
        stamina += stam;
	}

	public void PlayerThirst (float thirsty)
	{
        thirst += thirsty;
	}

	public void PlayerHealth (float hp)
	{
        health += hp;
	}

	public void PlayerEnergy (float ener)
	{
        energy += ener;
	}
}