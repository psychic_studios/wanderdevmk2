﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class k_itemDatabase : MonoBehaviour {

	public List <k_item> items = new List <k_item>();

	void Start()
	{
		items.Add (new k_item ("hatchet", "A tool for gathering wood.", 0, 2, k_item.ItemType.Tool));
	}
}
