﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class k_item {

	public string itemName;
	public string itemDesc;
	public int itemID;
	public Texture2D itemIcon;
	public int itemDmg;
	public ItemType itemType;

	public enum ItemType
	{
		Weapon,
		Armor,
		Consumable,
		Tool,
		Material,
		Trap
	}

	public k_item(string name, string description, int id, int damage, ItemType type)
	{
		itemName = name;
		itemDesc = description;
		itemID = id;
		itemIcon = Resources.Load<Texture2D>("Item Icons/" + itemName);
		damage = itemDmg;
		type = itemType;
	}
}