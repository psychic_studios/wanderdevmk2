﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {

    public Song currentSong;
    public Song nextSong;
    public Song[] songs;

    private PlayerStats ps;
    private AudioSource audio;

    void Start()
    {
        ps = GetComponent<PlayerStats>();

        audio = gameObject.AddComponent<AudioSource>();
        audio.loop = true;
        StartCoroutine(tick());
    }

    IEnumerator tick()
    {
        Song closest = null;
        foreach (Song s in songs)
        {
            if (closest == null || Mathf.Abs(ps.intensity - s.intensity) > closest.intensity)
            {
                closest = s;
            }
        }
        audio.clip = closest.song;
        audio.PlayOneShot(audio.clip);

        yield return new WaitForSeconds(1);
        StartCoroutine(tick());
    }
}
