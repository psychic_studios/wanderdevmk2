﻿using UnityEngine;
using System.Collections;

public class mouseFollow : MonoBehaviour {
    Vector3 movementvector;
	// Use this for initialization
	void Start () {
        InvokeRepeating("MoveCamera", 0.2F, 0.2F);
	}
	
    void MoveCamera()
    {
        transform.position += movementvector;
    }

	// Update is called once per frame
	void Update () {

        if (Input.mousePosition.x < Screen.width / 2)
            movementvector.x = -0.02F;
        if (Input.mousePosition.x > Screen.width / 2)
            movementvector.x = 0.02F;

        if (Input.mousePosition.y < Screen.height / 2)
            movementvector.y = -0.02F;
        if (Input.mousePosition.y > Screen.height / 2)
            movementvector.y = 0.02F;

    }
}
