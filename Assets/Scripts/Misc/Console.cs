﻿using UnityEngine;
using System.Collections;

public class Console : MonoBehaviour {

    public bool isOpen = false;
    public string command = "";

	void Update () {
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            isOpen = !isOpen;
        }

        if (isOpen)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                string[] cmd = command.Split(".".ToCharArray());
                GameObject.Find(cmd[0]).SendMessage(cmd[1]);
            }
        }
	}

    void OnGUI()
    {
        if (!isOpen)
        {
            return;
        }

        command = GUI.TextField(new Rect(10, 10, 200, 20), command);
    }
}
